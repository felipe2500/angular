export class DestinoViaje {
    private selected:boolean;
    public servicios: string[];
    constructor(public nombre:string, u:string){
        this.servicios = ['Piscina', 'Desayuno'];
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s:boolean) {
        this.selected = true;
    }
}