import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAuxComponent } from './lista-aux.component';

describe('ListaAuxComponent', () => {
  let component: ListaAuxComponent;
  let fixture: ComponentFixture<ListaAuxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAuxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
