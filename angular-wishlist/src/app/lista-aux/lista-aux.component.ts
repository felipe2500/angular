import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-aux',
  templateUrl: './lista-aux.component.html',
  styleUrls: ['./lista-aux.component.css']
})
export class ListaAuxComponent implements OnInit {
  destinos: DestinoViaje[];
  constructor() {
    this.destinos=[];
  }

  ngOnInit(){
  }

  guardar(nombre:string, url:string):boolean{
    this.destinos.push(new DestinoViaje(nombre, url));
    return false;
  }
  
  elegido(d : DestinoViaje) {
    this.destinos.forEach(function (x) {x.setSelected(false);});
    d.setSelected(true);
  }

}
